package main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Ejecutable {
    public static void main(String args[]){
        System.out.println("Encontrar un elemento dentro de un Stream.");
        //Collection
        List<User> users = new ArrayList<>();
        users.add(new User("Pedro", 23));
        users.add(new User("Maria", 32));
        users.add(new User("Lisa", 39));
        users.add(new User("Lourdes", 35));
        users.add(new User("Bart", 42));
        users.add(new User("Jose", 4));
        users.add(new User("Julia", 25));
        users.add(new User("Alejandra", 25));
        //Encontrar un elemento
        User userDefault = new User("Sin nombre", 30);
        User user = users.stream()
                .filter( u -> u.getAge() == 30)
                .findFirst() //.findAny()
                .orElse(userDefault); //.get();
        System.out.println(user.getName());
    }
}
